## KNXnet/IP API for NODE-RED-CONTRIB-KNX-ULTIMATE

Originarily written by Elias Karakoulakis.
This is a fork intended for use with Node-Red [KNX-Ultimate node](https://flows.nodered.org/node/node-red-contrib-knx-ultimate).

Please use the original [KNX.JS API by Elias Karakoulakis](https://www.npmjs.com/package/knx) for your projects instead.